# frozen_string_literal: true
require_relative "login_link/version"
require 'json'
require 'erb'
require 'jwt'
require 'uri'
require 'cgi'

module LoginLink
  class Error < StandardError; end
  class TokenMismatchError < StandardError; end

  class << self
    #
    # Configuration File format:
    # {
    #   "development": {
    #     "applications": {
    #       "app1": {
    #         "application": "App1",
    #         "url": "http://app1.example.com/jwt",
    #         "parameter_name": "token",
    #         "hmac_secret": "123456781234567812345678",
    #         "hmac_alg": "HS512",
    #         "exp": <%= 3600*24 %>,
    #         "encryption_key": "12345678123456781234567812345678",
    #         "encryption_alg": "AES-256-CBC",
    #         "token": "APP1"
    #       },
    #       "app2": {
    #         "application": "app2",
    #         "default": true,
    #         "url": "http://app2.example.com/login/jwt",
    #         "parameter_name": "token",
    #         "hmac_secret": "an hmac secret",
    #         "hmac_alg": "HS512",
    #         "exp": <%= 3600*24 %>, 
    #         "encryption key": "a super secret key 12345678",
    #         "encryption_alg": "AES-256-CBC",
    #         "token": "APP2"
    #       }
    #     }
    #   }
    def create_link(application: nil, user_identifier: nil, payload: {}, **args)
      app_config = configuration.dig("applications", application.to_s)

      if app_config.nil?
        raise LoginLink::Error.new("Unable to find configuration for app #{application}")
      end

      payload_data = {user_identifier: user_identifier,
                      from: default_config["application"],
                      token: app_config["token"],
                      payload: payload
                     }.to_json

      encrypted = false
      if app_config["encryption_key"]
        payload_data = encrypt(app_config["encryption_alg"], app_config["encryption_key"], payload_data)
        encrypted = true
      end

      jwt_data = JWT.encode({"payload" => Base64.encode64(payload_data), "encrypted" => encrypted},
                            app_config["hmac_secret"],
                            app_config["hmac_alg"])

      uri = update_uri(URI(app_config["url"]), args[:host], {app_config["parameter_name"] => jwt_data})
      uri.to_s
    end

    def parse(token)
      data, = JWT.decode token, default_config["hmac_secret"], true, { algorithm: default_config["hmac_alg"] }

      payload_data = Base64.decode64 data["payload"]
      if data["encrypted"] && default_config["encryption_key"] 
        payload_data = decrypt(default_config["encryption_alg"], 
                               default_config["encryption_key"],
                               payload_data)
      end

      payload = JSON.parse payload_data

      if (payload["token"] || default_config["token"]) &&
         (payload["token"] != default_config["token"])
        raise LoginLink::TokenMismatchError, "Tokens do not match"
      end

      payload
    end

    private

    def env
      @env ||= (Rails.env rescue nil) || ENV["RAILS_ENV"] || ENV["RACK_ENV"] || "development"
    end

    def load_config
      config_path = ENV["LOGIN_LINK_CONFIGFILE"] ||
                    File.join((Rails.root rescue Dir.pwd), "config/login_links.json")

      @configuration = JSON.parse ERB.new(File.read(config_path)).result
    rescue => e
      raise LoginLink::Error, "Unable to load login_link configuration: #{e.message}"
    end

    def configuration
      @configuration ||= load_config
      @configuration[env]
    end

    def default_config
      @default_config ||= begin
        defaults = configuration["applications"].select {|app, cfg| cfg["default"]}
        if defaults.count > 1
          raise LoginLink::Error, "mutliple default applications"
        elsif defaults.count == 0
          raise LoginLink::Error, "no default applications"
        end
        defaults.values.first
      end
    end

    def encrypt(alg, key, data)
      cipher = OpenSSL::Cipher.new(alg)
      cipher.encrypt
      cipher.key = key
      cipher.update(data) + cipher.final
    end

    def decrypt(alg, key, data)
      cipher = OpenSSL::Cipher.new(alg)
      cipher.decrypt
      cipher.key = key
      cipher.update(data) + cipher.final
    end

    def update_uri(uri, host, parms)
      url = uri.dup
      query_params = url.query ? CGI.parse(url.query) : {}
      query_params.merge!(parms)
      url.query = URI.encode_www_form(query_params)
      url.host = host unless host.nil?
      url
    end
  end
end
