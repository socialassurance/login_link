# frozen_string_literal: true
require 'byebug'
require "test_helper"
require "openssl"

class LoginLinkTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::LoginLink::VERSION
  end

  def test_end_to_end_flow
    ENV['LOGIN_LINK_CONFIGFILE'] = 'test/test_config.json'
    user = "email@example.com"
    payload = {"foo"=>"bar"}
    link = LoginLink.create_link(app: "good_app", user_identifier: user, payload: payload)

    assert link =~ /^http:\/\/example\.com\/llink/
    jwt_token = CGI.parse(URI(link).query)["jwt"][0]

    response = LoginLink.parse(jwt_token)

    assert_equal payload, response["payload"]
    assert_equal user, response["user_identifier"]
  end
end
